<?php
/**
 * 基于swoole的php后台守护进程
 * @package HuoKit
 *
 * @internal 基于swoole的php后台守护进程
 *
 * @author mawenpei
 * @date 2017/2/23 15:47
 * @version
 */
namespace HuoKit\JobMan\Core;
/**
 * Class PidManager
 * @package HuoKit\JobMan\Core
 */
class PidManager
{
    private $path;

    public function __construct($path)
    {
        $this->path = $path;
    }

    public function getMasterPid()
    {
        if(!file_exists($this->path)){
            return 0;
        }
        return intval(file_get_contents($this->path));
    }

    public function saveMasterPid($pid)
    {
        $pid = intval($pid);
        file_put_contents($this->path,$pid);
    }

    public function clear()
    {
        if(file_exists($this->path)) {
            unlink($this->path);
        }
    }
}