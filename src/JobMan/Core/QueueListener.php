<?php
/**
 * 基于swoole的php后台守护进程
 * @package HuoKit
 *
 * @internal 基于swoole的php后台守护进程
 *
 * @author mawenpei
 * @date 2017/2/23 15:47
 * @version
 */
namespace HuoKit\JobMan\Core;

use HuoKit\JobMan\Queue\BeanstalkdQueue;
use HuoKit\JobMan\Queue\IQueue;
use HuoKit\JobMan\Queue\Job;
use Psr\Log\LoggerInterface;
use swoole_process;

/**
 * Class QueueListener
 * @package HuoKit\JobMan\Core
 */
class QueueListener
{
    protected $workerName;

    protected $process;

    /**
     * @var IQueue
     */
    protected $queue;

    protected $logger;

    /**
     * @var ListenerStats
     */
    protected $stats;

    protected $times = 0;

    public function __construct($workerName, swoole_process $process,array $config,LoggerInterface $logger,ListenerStats $stats)
    {
        $this->workerName = $workerName;
        $this->process = $process;
        $this->config = $config;
        $this->logger = $logger;
        $this->stats = $stats;

        if(isset($config['queue'])){
            $this->queue = $config['queue'];
        }else{
            $this->queue = new BeanstalkdQueue($config);
        }
    }

    public function loop()
    {
        $worker = $this->createQueueWorker($this->workerName);
        while(true){
            $this->stats->touch($this->workerName,$this->process->pid,false,0);
            $stoping = $this->stats->isStoping();
            if ($stoping) {
                $this->logger->debug("process #{$this->process->pid} is exiting.");
                $this->process->exit(1);
                break;
            }

            $job = $this->reserveJob();

            if ($job===null) {
                $this->logger->debug('job is not exists');
                sleep(1);
                continue;
            }

            try {
                $result = $worker->execute($job);
                $this->stats->touch($this->workerName, $this->process->pid, false, 0);
                $this->logger->debug('worker execute success',[$job->getData()]);
            } catch(\Exception $e) {
                $message = sprintf('tube({$tubeName}, #%d): execute job #%d exception, `%s`', $this->process->pid, $job->getId(), $e->getMessage());
                $this->logger->error($message, ['data'=>$job->getData()]);
            }

            $code = is_array($result) ? $result['code'] : $result;
            $this->finishJob($job, $result);
        }
    }

    private function reserveJob()
    {
        $job = $this->queue->pop($this->workerName);
        $this->times ++;

        $jobId = $job ? $job->getId() : 0;
        $jobData = $job ? $job->getData() : null;

        $this->stats->touch($this->workerName, $this->process->pid, true,$jobId);

        if (!$job) {
            return null;
        }

        $this->logger->debug("worker ({$this->workerName}, #{$this->process->pid}): job #{$jobId} reserved.", ['data'=>$jobData]);

        return $job;
    }

    public function finishJob($job,$result)
    {
		try{
		    $this->queue->delete($job);
		}catch(\Exception $e){
            $this->logger->debug('delete job');
		}
    }

    private function createQueueWorker($name)
    {
        $class = $this->config['workers'][$name]['class'];
        $worker = new $class($name, $this->config['workers'][$name]);
        $worker->setLogger($this->logger);
        return $worker;
    }
}

