<?php
/**
 * 基于swoole的php后台守护进程
 * @package HuoKit
 *
 * @internal 基于swoole的php后台守护进程
 *
 * @author mawenpei
 * @date 2017/2/23 15:47
 * @version
 */
namespace HuoKit\JobMan\Handler;

use Psr\Log\LoggerInterface;

/**
 * Interface ITask
 * @package HuoKit\JobMan\Handler
 */
interface ITask
{
    public function handle();
    public function setLogger(LoggerInterface $logger);
}