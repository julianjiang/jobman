<?php
/**
 * 基于swoole的php后台守护进程
 * @package HuoKit
 *
 * @internal 基于swoole的php后台守护进程
 *
 * @author mawenpei
 * @date 2017/2/23 15:47
 * @version
 */
namespace HuoKit\JobMan\Queue;

use Pheanstalk\Pheanstalk;

/**
 * Class BeanstalkdQueue
 * @package HuoKit\JobMan\Queue
 */
class BeanstalkdQueue implements IQueue
{
    protected $client;

    public function __construct($config)
    {
        $host = isset($config['host']) ? $config['host'] : '127.0.0.1';
        $port = isset($config['port']) ? $config['port'] : 11300;
        $this->client = new Pheanstalk($host,$port);
    }

    public function put($name,$job)
    {
        return $this->client->putInTube($name,json_encode($job));
    }

    public function pop($name)
    {
        $job = $this->client->reserveFromTube($name,2);
        if($job){
            $jobId = $job->getId();
            $jobData = $job->getData();
            //$this->client->delete($job);
            return new Job($jobId,json_decode($jobData,true));
        }
        return $job;
    }

    public function delete($job)
    {
        return $this->client->delete($job);
    }
}