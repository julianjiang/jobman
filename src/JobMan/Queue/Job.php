<?php
/**
 * 基于swoole的php后台守护进程
 * @package HuoKit
 *
 * @internal 基于swoole的php后台守护进程
 *
 * @author mawenpei
 * @date 2017/2/23 15:47
 * @version
 */
namespace HuoKit\JobMan\Queue;
/**
 * Class Job
 * @package HuoKit\JobMan\Queue
 */
class Job implements IJob
{
    protected $id;
    protected $data;

    public function __construct($id,$data)
    {
        $this->id = $id;
        $this->data = $data;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getData()
    {
        return $this->data;
    }
}