<?php
/**
 * 基于swoole的php后台守护进程
 * @package HuoKit
 *
 * @internal 基于swoole的php后台守护进程
 *
 * @author mawenpei
 * @date 2017/2/23 15:47
 * @version
 */
namespace HuoKit\JobMan\Queue;
/**
 * Interface IJob
 * @package HuoKit\JobMan\Queue
 */
interface IJob
{
    public function getId();

    public function getData();
}