<?php
/**
 * 基于swoole的php后台守护进程
 * @package HuoKit
 *
 * @internal 基于swoole的php后台守护进程
 *
 * @author mawenpei
 * @date 2017/2/23 15:47
 * @version
 */
namespace HuoKit\JobMan;

use HuoKit\JobMan\Core\QueueListener;
use swoole_process;
use HuoKit\JobMan\Core\PidManager;
use HuoKit\JobMan\Core\ListenerStats;
use HuoKit\JobMan\Log\Logger;
use HuoKit\JobMan\Utility\ParseCrontab;
use HuoKit\JobMan\Utility\TickTable;

/**
 * Class Daemon
 * @package HuoKit\JobMan
 */
class Daemon
{
    private $config;

    protected $logger;

    protected $workers;

    protected $stats;

    protected $pidManager;

    protected $status;

    protected $master_process_name = 'jobman:master';

    protected static $unique_task_list;

    protected $tasks;

    public function __construct($config)
    {
        $this->config = $config;
        $this->pidManager = new PidManager($this->config['pid_path']);
        if(isset($this->config['logger'])){
            $this->logger = $this->config['logger'];
        }else{
            $this->logger = new Logger(['log_path'=>$this->config['log_path']]);
        }
    }

    /**
     * 开启服务
     */
    public function start()
    {
        if($this->pidManager->getMasterPid()){
            echo 'Error:JobMan is already running' . "\r\n";
            return;
        }
        echo 'JobMan started' . "\r\n";
        if($this->config['daemonize']){
            swoole_process::daemon();
        }

        if(isset($this->config['process_name'])){
            $this->master_process_name = $this->config['process_name'];
        }

        $this->stats = $this->createListenerStats();

        $this->loadCrontabRule();

        swoole_set_process_name($this->master_process_name);

        $this->workers = $this->createWorkers($this->stats);

        $this->registerTimer();

        $this->registerSignal();

        $this->pidManager->saveMasterPid(posix_getpid());

        swoole_timer_tick(1000,function($timerId){
            $statses = $this->stats->getAll();

            foreach($statses as $pid=>$s){
                if ( ($s['last_update'] + $this->config['reserve_timeout'] + $this->config['execute_timeout']) > time()) {
                    continue;
                }
                if (!$s['timeout']) {
                    $this->logger->info("process #{$pid} last upadte at ". date('Y-m-d H:i:s') . ', it is timeout.', $s);
                    $this->stats->timeout($pid);
                }
            }
        });

    }

    /**
     * 停止服务
     */
    public function stop()
    {
        $pid = $this->pidManager->getMasterPid();
        if (empty($pid)) {
            echo "JobMan is not running...\n";
            return ;
        }

        echo "JobMan is stoping....\n";

        if(swoole_process::kill($pid,0)){
            swoole_process::kill($pid,SIGTERM);
        }else{
            $this->pidManager->clear();
        }
        while(1) {
            if ($this->pidManager->getMasterPid()) {
                sleep(1);
                continue;
            }

            echo "[OK]\n";
            break;
        }

    }

    /**
     * 重启服务
     */
    public function restart()
    {
        $this->stop();
        sleep(1);
        $this->start();
    }

    /**
     * 创建监听器状态表
     * @return ListenerStats
     */
    private function createListenerStats()
    {
        $size = 0;
        foreach($this->config['workers'] as $workerName=>$workerConfig){
            $size += $workerConfig['worker_num'];
        }
        return new ListenerStats($size,$this->logger);
    }

    /**
     * 加载Crontab执行规则
     */
    private function loadCrontabRule()
    {
        $time = time();

        foreach($this->config['crontabs'] as $name=>$config){
            if(!isset($config['rule'])) continue;
            $ret = ParseCrontab::parse($config['rule'],$time);
            if($ret === false){
                $this->logger->error(ParseCrontab::$error);
            }elseif(!empty($ret)){
                $config['id'] = 0;
                $config['name'] = $name;
                TickTable::set_task($ret,$config);
            }
        }
    }

    private function loadTasks($timer_id)
    {
        $tasks = TickTable::get_task();
        if(empty($tasks)) return false;
        foreach($tasks as $task){
            if(isset($task['unique']) && $task['unique']){
                if (isset(static::$unique_task_list[$task['id']]) && (static::$unique_task_list[$task['id']] >= $task['unique'])) {
                    continue;
                }
                static::$unique_task_list[$task['id']] = isset(static::$unique_task_list[$task['id']]) ? (static::$unique_task_list[$task['id']] + 1) : 0;
            }

            $this->createTaskProcess($task['id'],$task);
        }
    }

    private function createTaskProcess($id,$task)
    {
        $process = new swoole_process(function($worker)use($id,$task){
            $worker->name($this->master_process_name . ':' . $task['name'] .':child-crontab-process');
            $class = $task['class'];
            $man = new $class();
            $man->setLogger($this->logger);
            $man->handle();
            $worker->exit(1);
        });
        $pid = $process->start();
        $this->tasks[$pid] = $process;
    }

    private function registerTimer()
    {
        swoole_timer_tick(1000,function($timer_id){
            $this->loadTasks($timer_id);
        });

        swoole_timer_tick(60000,function(){
            $this->loadCrontabRule();
        });
    }

    /**
     * 创建工作进程
     * @param $stats
     * @return array
     */
    private function createWorkers($stats)
    {
        $workers = [];
        foreach ($this->config['workers'] as $workerName => $workerConfig) {
            for($i=0; $i<$workerConfig['worker_num']; $i++) {
                $worker = new swoole_process($this->createQueueLoop($workerName, $stats), true);
                $worker->start();
                $workers[$worker->pid] = $worker;
            }
        }

        foreach($workers as $pid=>$worker){
            swoole_event_add($worker->pipe, function($pipe) use ($worker) {
                $recv = $worker->read();
            });
        }

        return $workers;
    }

    /**
     * 任务队列监听器
     * @param $workerName
     * @param $stats
     * @return \Closure
     */
    private function createQueueLoop($workerName,$stats)
    {
        return function($process) use ($workerName, $stats) {
            $process->name($this->master_process_name . ':'.$workerName.':child-worker-process');
            //创建任务监听器
            $listener = new QueueListener($workerName, $process, $this->config, $this->logger, $stats);
            $listener->loop();
        };
    }

    /**
     * 注册信号
     */
    private function registerSignal()
    {
        //终止子进程
        swoole_process::signal(SIGCHLD,function(){
            while($ret = swoole_process::wait(false)){

                if(isset($this->workers[$ret['pid']])){
                    $this->logger->info('process #' . $ret['pid'] . ' exited',$ret);
                    $this->workers[$ret['pid']]->close();
                    unset($this->workers[$ret['pid']]);
                    $this->stats->remove($ret['pid']);
                }

                if(isset($this->tasks[$ret['pid']])){
                    $this->logger->info('process #' . $ret['pid'] . ' exited',$ret);
                    $this->tasks[$ret['pid']]->close();
                    unset($this->tasks[$ret['pid']]);
                }
            }
        });

        //终止进程时等待子进程回收并执行后续清理工作
        $softkill = function($signo){
            if($this->status == 'stoping'){
                return;
            }
            $this->status = 'stoping';
            $this->logger->info('JonMan is stoping....');
            $this->stats->stop();

            swoole_timer_tick(1000,function($timerId){
                if(!empty($this->workers)){
                    return;
                }

                if(!empty($this->tasks)){
                    return;
                }

                //清除定时器
                swoole_timer_clear($timerId);
                //清除主进程pid文件
                $this->pidManager->clear();
                $this->logger->info('JobMan is stopeed');
                exit();
            });
        };
        //终止进程
        swoole_process::signal(SIGTERM,$softkill);
        swoole_process::signal(SIGINT,$softkill);
    }
}