## 基于swoole的php后台守护进程

### 适用场景
1. web中较慢的可异步处理的逻辑，比如：统计/短信/图片处理等
1. 需要多线程并发处理的逻辑，比如：页面抓取、消息、推送等
### 特性
1. 基于swoole的job调度组件
1. 基于Beanstalk队列的消息存储
1. 利用swoole的process实现多进程管理，进程个数可配置，worker进程退出后会自动拉起
1. 子进程循环次数可配置，防止业务代码内存泄漏
1. 支持秒级的定时任务,可用于替换操作系统crontab
1. 支持composer，可以跟任意框架集成
### 安装方式
把下面的json代码加到composer.json里,然后执行composer install，需要按照swoole的php扩展

```json

{
	"require":{
		"julianjiang/jobman":"dev-master"
	},
	"repositories":[
		{
		    "type":"git",
			"url":"https://git.oschina.net/julianjiang/jobman.git"
		}
    ]
}

```

### 使用方式
#### 配置文件

```php

return [
    'host'=>'host',
    'port'=>11300,
    'process_name'=>'jobman:master',
    'workers'=>[
        'demo_worker'=>[
            'worker_num'=>5,
            'class'=>'\\demoWorker'
        ]
    ],
    'crontabs'=>[
        'crontab_second_task'=>[
            'rule'=>'* * * * * *',
            'class'=>'\\demoTask'
        ]
    ],
    'log_path'=>__DIR__ . '/jobman.log',
    'pid_path'=>__DIR__ . '/jobman.pid',
    'daemonize'=>1,
    'reserve_timeout'=>5,
    'execute_timeout'=>60
];

```
